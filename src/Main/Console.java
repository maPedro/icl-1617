package Main;

import java.io.ByteArrayInputStream;
import Parser.ParseException;
import Parser.Parser;

public class Console {

	@SuppressWarnings("static-access")
	public static void main(String args[]) {
		Parser parser = new Parser(System.in);
	
	    while (true) {
		    try {
		      parser.Start();
		      System.out.println( "OK!" );
		    } catch (ParseException e) {
		    	System.out.println ("Syntax Error!");
		    	e.printStackTrace();
		    	parser.ReInit(System.in);
		    } 
	    }
	}
	
	public static boolean accept(String s) throws ParseException {
		Parser parser = new Parser(new ByteArrayInputStream(s.getBytes()));
	    try {
	    	parser.Start();
	    	return true;
	    } catch (ParseException e) {
	    	return false;
	    } 
	}

}
